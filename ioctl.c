#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

int main(void){
	struct winsize size;
	ioctl(STDIN_FILENO, TIOCGWINSZ, &size);
	printf("(%d  %d)\n", size.ws_row, size.ws_col);
}

