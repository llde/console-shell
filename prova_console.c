#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <wchar.h>
#include <ctype.h>
#include <locale.h>
#include <termios.h>
#include <unistd.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))





struct termios oooold;

void texit(void){
    tcsetattr(STDIN_FILENO, TCSANOW, &oooold);
}


int main(void){
 //   setlocale(LC_ALL, "it_IT.utf8");
    struct termios oldattr,newattr;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
  //  newattr.c_lflag &= ~(ICANON | IEXTEN);
  //  newattr.c_iflaf & = ~ISTRIP;
    cfmakeraw(&newattr);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr) ;
    int current_alloc = 512;
    oooold = oldattr;
    atexit(texit);
    int length = 0;  //The apparent string length where multi-byte char are counted as 1
    int current_index = 0; //The apparent cursor position×
    int real_length = 0;  //The real length 
    int real_current_index = 0;
    unsigned short* num_byte_for_char = calloc(512, sizeof(unsigned short));
    char* command = calloc(512, sizeof(char));
    fprintf(stdout, "\x1B[1000D");
    fflush(stdout);
    while (1) {
	memset(command, 0, current_alloc);
	printf("Inserisci cose:\n");
        fprintf(stdout, "\x1B[1000D");
        fflush(stdout);
	int multi = 0;
	int ccc;
	while(1){
	    ccc = getchar();
	    if(ccc == EOF) break;
	    else if(ccc == 3) exit(0); //This ic CTRL-C
	    else if(ccc == '\n'  || ccc == '\r'){
                fprintf(stdout, "\x1B[1000D");
		fprintf(stdout, "\nechoing %s\n", command);
		current_index = 0;
		length= 0;
		memset(command,0,current_alloc);
	    }
	    else if(ccc == '\x1B'){
	    	char ccc1 = getchar();
		char ccc2 = getchar();
		if(ccc1 == '['){
		    if(ccc2 == 'D') current_index = MAX(0, current_index -1);
		    else if (ccc2 == 'C') current_index =  MIN(length, current_index +1);
		}
	    }
	    else if(ccc == 8) {   //Backspace
		if(current_index != 0){
		    if(length == current_index){
	                command[current_index -1] = '\0';
		        current_index--;
		        length--;
		    }
	 	    else{
		        memmove(command + current_index -1, command + current_index, length - current_index);
		        command[length -1] = '\0';
		        length--;
		        current_index--;
	            }
		}
	    }
	     else if( ccc >=32 && ccc <= 126){  //ASCII chars.
                if(current_index  == length){
                    command[real_current_index] = ccc;
		    num_byte_for_char[current_index] = 1;
                    current_index++;
		    real_current_index++;
		    real_length++;
                    length++;
                }
                else{
                    char* interm = command + real_current_index;
                    memmove(interm +1, interm, real_length - real_current_index);
		    memmove(num_byte_for_char + current_index +1, num_byte_for_char + current_index, length - current_index );
                    *interm = ccc;
                    length++;
		    real_current_index++;
                    current_index++;
		    real_length++;
                }
            }
	    else if(ccc >= 127) { //Extended ASCII chars
	    	printf("  %d %c\n", ccc);
	    }
	    else{
		    printf("%c   %d", ccc, ccc);
	    }
            fprintf(stdout, "\x1B[1000D");
	    fprintf(stdout, "\x1B[0K");
	    fprintf(stdout, "%s", command);
	    fprintf(stdout, "\x1B[1000D");
	    if(current_index > 0) fprintf(stdout, "\x1B[%dC", current_index);
	    fflush(stdout);
//TODO up and down
	}
//	printf("%s\n", c);
    }
}
