#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <stdio.h>
#include <wchar.h>
/* Return a new file descriptor to the current TTY.
*/
int current_tty(void)
{
    const char* dev;
    
    dev = ttyname(STDIN_FILENO);
    if (!dev)
        dev = ttyname(STDOUT_FILENO);
    if (!dev)
        dev = ttyname(STDERR_FILENO);
    if (!dev) {
        errno = ENOTTY;
        return -1;
    }
    return open(dev, O_RDWR | O_NOCTTY);
}



#define   RD_EOF   -1
#define   RD_EIO   -2
static inline ssize_t rd(const int fd, unsigned char* buf, size_t num_bytes){
    ssize_t remains = num_bytes;
    unsigned char* read_buf = buf;
    while (remains > 0) {
        ssize_t n = read(fd, read_buf, remains);
        if (n > (ssize_t)0){
            remains -= n;
	    read_buf = read_buf + n;
    	}
        else if (n == (ssize_t)0)
            return RD_EOF;

        else if (n != (ssize_t)-1)
            return RD_EIO;

        else if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK)
            return RD_EIO;
    }
    return num_bytes;
}



static inline int wr(const int fd, const char* const data, const size_t bytes)
{
    const char*  last_written = data;  
    size_t  missing_bytes = bytes;
    while (missing_bytes > 0) {
        ssize_t n = write(fd, last_written, missing_bytes);
        if (n > (ssize_t)0){
            missing_bytes -= n;
	    last_written = last_written + n;
	}
        else if (n != (ssize_t)-1)
            return EIO;

        else if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK)
            return errno;
    }

    return 0;
}




int main_b(void){
    int tty = current_tty();
    if(tty == -1) return 3;
    struct termios oldsettings, newsettings;
    int result = tcgetattr(tty, &oldsettings);
    newsettings = oldsettings;
    cfmakeraw(&newsettings);
    tcsetattr(tty,TCSANOW , &newsettings);
    printf("\x1B[1000C");
    printf("\x1B[1000B");
    fflush(stdout);
     /* Request cursor coordinates from the terminal. */
    int retval = wr(tty, "\033[6n", 4);
    if (retval)
        goto err;

    /* Assume coordinate reponse parsing fails. */
    retval = EIO;
    unsigned char  buf[2] = {0}; 
    result = rd(tty, buf, 1);
    if (buf[0] != 27)
        goto err;

     /* Expect [ after the ESC. */
    result = rd(tty, buf, 1);
    if (buf[0] != '[')
        goto err;

    /* Parse rows. */
    int rows = 0;
    result = rd(tty, buf, 1);
    while (buf[0] >= '0' && buf[0] <= '9') {
        rows = 10 * rows + buf[0] - '0';
        result = rd(tty, buf, 1);
    }

    if (buf[0] != ';')
        goto err;

    /* Parse cols. */
    int cols = 0;
    result = rd(tty, buf, 1);
    while (buf[0] >= '0' && buf[0] <= '9') {
        cols = 10 * cols + buf[0] - '0';
        result = rd(tty, buf, 1);
    }

    if (buf[0] != 'R')
        goto err;

    /* Success! */
    printf("%d   %d", rows, cols);
    retval = 0;
err:    tcsetattr(tty ,TCSANOW,&oldsettings);
    
    return retval;
}

int main(void){
    struct termios oldsettings, newsettings;
    int result = tcgetattr(STDIN_FILENO, &oldsettings);
    newsettings = oldsettings;
    cfmakeraw(&newsettings);
    tcsetattr(STDIN_FILENO,TCSANOW , &newsettings);
    fprintf(stdout, "\x1B[1000C");
    fprintf(stdout, "\x1B[1000B");
    fprintf(stdout, "\x1B[6n");
    fflush(stdout);
    int rows = 0, columns = 0;
    int escape_detected = 0;
    while(escape_detected < 4){
	wchar_t t = getwchar();
        if(t == L'\x1B'  && escape_detected == 0)escape_detected = 1;
	else if(escape_detected == 1 && t == L'[') escape_detected = 2;
	else if(escape_detected == 2 && t == L';') escape_detected = 3;
	else if(escape_detected == 3 && t == L'R') escape_detected = 4;
	else if(escape_detected != 3 && t == L'R') escape_detected = 5;
	else if(escape_detected == 2 && t >= L'0' && t <= L'9' ){
	     rows = (rows * 10) + t -L'0';
	}
	else if(escape_detected == 3 && t >= L'0' && t <= L'9'){
	     columns = columns * 10 + t - L'0';
	}
	else escape_detected = 5;
	printf("%lc\n", t);
    }
    fprintf(stdout, "\x1B[1000D");
    fprintf(stdout, "%d\n", escape_detected);
    fprintf(stdout, "(%d, %d)  \n", rows, columns);
    tcsetattr(STDIN_FILENO,TCSANOW , &oldsettings);
    return 0;
}

