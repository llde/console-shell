#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <wchar.h>
#include <ctype.h>
#include <locale.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

FILE* tty_fd;
struct termios oooold;
int get_cur_pos(int* row, int* column){
 //   fflush(stdout);
 //   fflush(stdin);
    char* p = ttyname(STDIN_FILENO);
    int fd = open(p, O_RDWR | O_NOCTTY );
    if(fd < 0) perror("PorcoDDIO");
    tcflush(fd, TCIOFLUSH);
    write(fd, "\x1B[6n", 4);
    //fflush(stdout);
    int escape_detected = 0, rows = 0, columns = 0;
    while(escape_detected < 4){
	char buf[1] = {0};
        int res = read(fd, buf, 1);
	if(res == -1) perror("Morire");
	int t = buf[0];
	if(t == EOF) return -1;
	else if(t == '\x1B'  && escape_detected == 0)  escape_detected = 1;
        else if(escape_detected == 1 && t == '[') escape_detected = 2;
        else if(escape_detected == 2 && t == ';') escape_detected = 3;
        else if(escape_detected == 3 && t == 'R') escape_detected = 4;
        else if(escape_detected != 3 && t == 'R') escape_detected = 5;
        else if(escape_detected == 2 && t >= '0' && t <= '9' ){
            rows = (rows * 10) + t -'0';
        }
        else if(escape_detected == 3 && t >= '0' && t <= '9'){
            columns = (columns * 10) + t - '0';
        }
        else{
	  //  escape_detected = 5;
	   //if(escape_detected == 0) continue;
//            fprintf(stdout, "\x1B[1000D");
//            fflush(stdout);
	    errno = EINVAL;
//	    printf("%c     escape %d\n", t, escape_detected);
	    escape_detected = 5;
	}
    }
    tcflush(fd, TCIOFLUSH);
    close(fd);
    if(row != NULL) *row =  rows;
    if(column != NULL) *column = columns;
  //  fflush(stdin);
   // fflush(stdout);
    return escape_detected == 4 ? 0  : -1;
}

int get_max_screen_position(int* row, int* column){
     struct winsize size;
     ioctl(STDIN_FILENO, TIOCGWINSZ, &size);
     if(row != NULL) *row = size.ws_row;
     if(column != NULL) *column = size.ws_col;
     return 0;
}
void texit(void){
    tcsetattr(STDIN_FILENO, TCSANOW, &oooold);
}


int main(void){
    fd_set readset;
 //   FD_ZERO(&readset);
 //   setvbuf(stdin, NULL, _IONBF, 0);
 //   FD_SET(STDIN_FILENO, &readset);
    setlocale(LC_ALL, "en_US.utf8");
    struct termios oldattr,newattr;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
  //  newattr.c_lflag &= ~(ICANON | IEXTEN);
  //  newattr.c_iflaf & = ~ISTRIP;
    cfmakeraw(&newattr);
    newattr.c_cc[VTIME] = 0;
    newattr.c_cc[VMIN] =  1;
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr) ;
    int current_alloc = 512;
    oooold = oldattr;
    atexit(texit);
    int length = 0;  
    int current_index = 0;
    int back_max_col = 0, back_max_row = 0;
    wchar_t* command = calloc(512, sizeof(wchar_t));
    fprintf(stdout, "\x1B[1000D");
    fflush(stdout);
    while (1) {
	memset(command, 0, current_alloc * sizeof(wchar_t));
	printf("Inserisci cose:\n");
        fprintf(stdout, "\x1B[1000D");
        fflush(stdout);
	int old_lines = 0, old_columns = 0, old_out_col = 0, old_out_row = 0;
	int ccc;
	while(1){
	    ccc = getwchar();
	    if(ccc == WEOF) break;
	    else if(ccc == 3) exit(0); //This ic CTRL-C
	    else if(ccc == L'\n'  || ccc == L'\r'){
                fprintf(stdout, "\x1B[1000D");
		fprintf(stdout, "\nechoing %ls\n", command);
		current_index = 0;
		length= 0;
		memset(command,0,current_alloc* sizeof(wchar_t));
	    }
	    else if(ccc == L'\x1B'){
	    	wchar_t ccc1 = getwchar();
		wchar_t ccc2 = getwchar();
		if(ccc1 == L'['){
		    if(ccc2 == L'D') current_index = MAX(0, current_index -1);
		    else if (ccc2 == L'C') current_index =  MIN(length, current_index +1);
		}
	    }
	    else if(ccc == 8 || ccc == 127) {   //Backspace
		if(current_index != 0){
		    if(length == current_index){
	                command[current_index -1] = '\0';
		        current_index--;
		        length--;
		    }
	 	    else{
		        memmove(command + current_index -1, command + current_index, (length - current_index) * sizeof(wchar_t));
		        command[length -1] = '\0';
		        length--;
		        current_index--;
	            }
		}
		else{
		     fprintf(stdout, "\a");
		     fflush(stdout);
		}
	     }
	     else if( ccc >=32){  //ASCII chars.
		if(length + 1 == 512) continue;
                if(current_index  == length){
                    command[current_index] = ccc;
		    current_index++;
                    length++;
                }
                else{
                    wchar_t* interm = command + current_index;
                    memmove(interm +1, interm, (length - current_index)* sizeof(wchar_t));
		    *interm = ccc;
                    length++;
                    current_index++;
		}
            }
	    else{
		    printf("%c   %d", ccc, ccc);
	    }
	    int max_pos_col = 0, max_row = 0;
	    int back_max_col = max_pos_col, back_max_row = max_row;
	    if(get_max_screen_position(&max_row,&max_pos_col) != 0){
		max_pos_col = back_max_col;
		max_row = back_max_row;
	    }          
	    int curr_col = 0, curr_row = 0, curr_pos_row_out = 0, curr_pos_col_out = 0;
	    curr_col = (current_index) % max_pos_col;
	    curr_row = (current_index) / max_pos_col;
	    curr_pos_row_out = (length) / max_pos_col;
	    curr_pos_col_out = (length) % max_pos_col;
	    int old_difference = old_out_row - old_lines;
            int difference = curr_pos_row_out - curr_row;
	//    if(curr_row == 1) {
	//         printf("Arrivato");
	//	 fflush(stdout);
	//	 sleep(4);
	 //   }
            //printf("(%d  %d)\n", curr_col, curr_row);
	    if(old_difference > 0){ 
		fprintf(stdout, "\x1B[%dB", old_difference);
	        fflush(stdout);
	        //sleep(4);
	    }
	    for(int i= 0; i < old_lines + old_difference +1; i++){	
   	        if(i != 0) fprintf(stdout, "\x1B[1A");
		fprintf(stdout, "\x1B[2K");
	        fflush(stdout);	 
	    }
	    fprintf(stdout, "\x1B[1000D");
	    fprintf(stdout, "%ls\n", command);
	    fprintf(stdout, "\x1B[1A");
	    fprintf(stdout, "\x1B[1000D");
	    fflush(stdout);
	    if(curr_pos_row_out > 0 && curr_pos_col_out > 0) {
	         fprintf(stdout, "\x1B[%dA", curr_pos_row_out);
	    }
	    else if(curr_pos_row_out -1 > 0){
	         fprintf(stdout, "\x1B[%dA", curr_pos_row_out -1);
	    }
	    fflush(stdout);

	    if(curr_col > 0) fprintf(stdout, "\x1B[%dC", curr_col);
	    if(curr_row > 0) fprintf(stdout, "\x1B[%dB", curr_row);
	    
	    old_lines = curr_row;
	    old_out_row = curr_pos_row_out;
            old_out_col = curr_pos_col_out;
	    fflush(stdout);
//TODO up and down
	}
//	printf("%s\n", c);
    }
}
